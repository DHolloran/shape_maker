package com.holloran.shapes
{
	import flash.display.Sprite;

	public class MyShape extends Sprite
	{

		private var _color:uint;
		private var _strokeColor:uint;
		private var _strokeWidth:uint;

		public function MyShape()
		{
			super ();
			drawShape ();

		}

		private function drawShape():void
		{

			//Body
			this.graphics.lineStyle ( 4, 0xCCCCCC ); //Body Linestyle
			this.graphics.moveTo ( 10, 5 ); //Starts Body
			this.graphics.lineTo ( 10, 25 ); //Ends Body
			this.graphics.lineTo ( 15, 45 ); //Right Leg
			this.graphics.lineTo ( 18, 45 ); //Right Foot
			this.graphics.moveTo ( 10, 25 ); //Start Left Leg
			this.graphics.lineTo ( 5, 45 ); //End Left Leg
			this.graphics.lineTo ( 2, 45 ); //Left Foot
			this.graphics.moveTo ( 10, 15 ); //Start Right Hand
			this.graphics.lineTo ( 20, 20 ); //End Right Hand
			this.graphics.moveTo ( 10, 15 );
			this.graphics.lineTo ( 0, 20 );

			//Head
			this.graphics.lineStyle ( 1, 0xCCCCCC ); //Start Head
			this.graphics.beginFill ( 0xCCCCCC );
			this.graphics.drawCircle ( 10, 0, 10 );
			this.graphics.endFill (); //End Head

			//Eyes
			this.graphics.lineStyle ( 0, 0x000000 );
			this.graphics.beginFill ( 0x000000 );
			this.graphics.drawCircle ( 15, 0, 2 ); //Right Eye
			this.graphics.drawCircle ( 5, 0, 2 ); //Left Eye
			this.graphics.endFill ();

		}
	}
}