package
{
	import com.holloran.shapes.MyShape;

	import flash.display.Sprite;
	import flash.events.MouseEvent;

	import libs.removeShapesBTN;

	[SWF( width="600", height="400", backgroundColor="0xFFFFFF", frameRate="24" )]
	public class Main extends Sprite
	{
		private var _canvas:Sprite;
		private var _shapes:Array;

		public function Main()
		{
			super ();
			_shapes = [];
			setup ();

		}

		private function createRemoveBtn():void
		{
			var myBtn:removeShapesBTN = new removeShapesBTN ();
			myBtn.x = stage.stageWidth - ( myBtn.width + 50 );
			myBtn.y = stage.stageHeight - ( myBtn.height + 50 );
			myBtn.mouseChildren = false;
			myBtn.buttonMode = true;
			myBtn.tfRemove.selectable = false;
			this.addChild ( myBtn );
			myBtn.addEventListener ( MouseEvent.CLICK, removeShapes );
			myBtn.addEventListener ( MouseEvent.MOUSE_OVER, onOver );
			myBtn.addEventListener ( MouseEvent.MOUSE_OUT, onOut );

		}

		private function onOver( e:MouseEvent ):void
		{
			stage.removeEventListener ( MouseEvent.MOUSE_DOWN, createShape )

		}

		private function onOut( e:MouseEvent ):void
		{
			stage.addEventListener ( MouseEvent.MOUSE_DOWN, createShape )

		}

		private function createShape( e:MouseEvent ):void
		{
			//Creates a new shape and adds it to _shapes
			var myShape:MyShape = new MyShape ();
			myShape.x = e.stageX;
			myShape.y = e.stageY;
			_canvas.addChild ( myShape );
			_shapes.push ( myShape );

		}

		private function removeShapes( e:MouseEvent ):void
		{

			if ( _shapes.length >= 1 )
			{
				for each ( var shape:MyShape in _shapes )
				{
					_canvas.removeChild ( shape );
				}

				_shapes = [];
			}


		}

		private function setup():void
		{
			_canvas = new Sprite ();
			this.addChild ( _canvas );

			//Click on stage and draw shape there
			stage.addEventListener ( MouseEvent.MOUSE_DOWN, createShape );

			createRemoveBtn ();

		}
	}

}
